package Simulation;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * defines a queue in the simulation environment
 */
public class Queue implements Runnable {

    private int qid;
    private ArrayList<Client> clients;
    private int waitTime; //average waiting time
    private int emptyTime;
    //private boolean empty;
    private int nbC = 0;

    /**
     * constructor
     *
     * @param qid queue id
     */
    public Queue(int qid) {
        this.qid = qid;
        this.clients = new ArrayList<Client>();
        this.waitTime = 0;
        this.emptyTime = -1;
    }

    /**
     * id getter
     *
     * @return id
     */
    public int getId() {
        return qid;
    }

    /**
     * returns the size of the queue
     *
     * @return queue size
     */
    public synchronized int size() {
        notifyAll();
        return clients.size();
    }

    /**
     * waited time getter
     *
     * @return wait time
     */
    public int getWaitTime() {
        return waitTime;
    }

    /**
     * empty time getter
     *
     * @return empty time
     */
    public int getEmptyTime() {
        return emptyTime;
    }

    /**
     * increments empty time
     */
    public void setEmptyTime() {
        this.emptyTime++;
    }

    /**
     * returns the reference to the client list of the queue
     *
     * @return client list
     */
    public ArrayList<Client> getClients() {
        return clients;
    }

    /**
     * adds a client to the queue
     *
     * @param c client to add
     */
    public synchronized void addClient(Client c) {
        clients.add(c);
        notifyAll();
    }

    /**
     * empty queue query
     *
     * @return true if queue empty, false otherwise
     */
    public boolean isEmpty() {
        return clients.size() == 0;
    }

    /**
     * removes a client from the queue
     *
     * @return removed client
     * @throws InterruptedException thread interrupted
     */
    public synchronized Client removeClientFromQ() throws InterruptedException {
        if (clients.isEmpty()) {
            wait();
            //throw new InterruptedException("Finished!");
        }
        Client c = clients.get(0);
        clients.remove(c);
        notifyAll();
        return c;
    }

    public String toString() {

        String que = new String(" ");
        for (Iterator<Client> itc = clients.iterator(); itc.hasNext(); ) {
            Client cl = itc.next();
            que = que + "C" + cl.getId() + " ";
        }
        return que;
    }

    @Override
    public void run() {

        while (true) {
            try {
                if (!clients.isEmpty()) {

                    Thread.sleep(clients.get(0).getServiceTime() * 1000);
                    Client cl = removeClientFromQ();
                    nbC++;
                    waitTime += cl.getServiceTime() / nbC;
                } //else setEmptyTime();
                Thread.sleep(10);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }


    }

}
