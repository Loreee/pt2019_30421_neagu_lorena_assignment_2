package Simulation;

/**
 * defines a client in the simulation environment
 */
public class Client implements Comparable {

    private int id;
    private int arrivalTime;
    private int serviceTime;
    private int waitingTime;
    private int finishTime;

    /**
     * constructor
     *
     * @param id          clients id
     * @param arrivalTime clients arrival time
     * @param serviceTime clients serving time
     */
    public Client(int id, int arrivalTime, int serviceTime) {
        this.id = id;
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
        this.waitingTime = 0;
        this.finishTime = 0;
    }

    /**
     * id getter
     *
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * arrival time getter
     *
     * @return arrival time
     */
    public int getArrivalTime() {
        return arrivalTime;
    }

    /**
     * waiting time getter
     *
     * @return waiting time
     */
    public int getWaitingTime() {
        return this.waitingTime;
    }

    /**
     * finish time getter
     *
     * @return finish time
     */
    public int getFinishTime() {
        return this.finishTime;
    }

    /**
     * serving time getter
     *
     * @return serving time
     */
    public int getServiceTime() {
        return serviceTime;
    }

    /**
     * waiting time setter
     *
     * @param wainting waiting time
     */
    public void setWaitingTime(int wainting) {
        this.waitingTime = wainting;
    }

    /**
     * finish time setter
     *
     * @param finish finish time
     */
    public void setFinishTime(int finish) {
        this.finishTime = finish;
    }

    @Override
    public String toString() {
        return "Client: " + id + " Arrival Time: " + arrivalTime + "  Processing Time: " + serviceTime;
    }

    @Override
    public int compareTo(Object o) {
        if (this == o)
            return 0;
        Client that = (Client) o;
        if (this.arrivalTime < that.arrivalTime)
            return -1;
        return +1;
    }

}
