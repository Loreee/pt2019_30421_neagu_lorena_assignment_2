package Controller;

import Exceptions.WrongValuesException;
import Simulation.Simulation;
import View.QView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * GUI Controller
 */
public class QController {

    private Simulation simulation;
    private QView view;

    int min_arrival;
    int max_arrival;
    int min_service;
    int max_service;
    ;
    int nb_Clients;
    int simulation_Time;
    private int nb_Cases;

    /**
     * constructor
     *
     * @param sim  simulation ref
     * @param view view ref
     */
    public QController(Simulation sim, QView view) {
        this.simulation = sim;
        this.view = view;

        view.addEnterListener(new EnterListener());
        view.addStartListener(new StartListener());
    }

    /**
     * defines functionality when enter button is pressed
     */
    class EnterListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                min_arrival = Integer.parseInt(view.min_arrival.getText());
                max_arrival = Integer.parseInt(view.max_arrival.getText());
                min_service = Integer.parseInt(view.min_processing.getText());
                max_service = Integer.parseInt(view.max_processing.getText());
                nb_Cases = Integer.parseInt(view.nr_servers.getText());
                nb_Clients = Integer.parseInt(view.nr_clients.getText());
                simulation_Time = Integer.parseInt(view.incrementTime.getText());
                view.qs_text = view.initializeQueues(nb_Cases);

                if (min_arrival > max_arrival || min_service > max_service || nb_Cases > 7
                        || max_arrival > simulation_Time || max_service > simulation_Time)
                    throw new WrongValuesException("Values not valid");

            } catch (WrongValuesException e1) {
                view.showError(e1.getMessage());
            }
        }
    }

    /**
     * defines functionality when start button is pressed
     */
    class StartListener implements ActionListener {
        public void actionPerformed(ActionEvent a) {

            simulation = new Simulation(view, min_arrival, max_arrival, min_service, max_service, nb_Clients, nb_Cases,
                    simulation_Time);
            Thread t = new Thread(simulation);
            t.start();

        }
    }

}
