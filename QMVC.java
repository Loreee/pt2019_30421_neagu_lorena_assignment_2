package MVC;
import java.io.*;

import javax.swing.*;

import Controller.QController;
import Simulation.Simulation;
import View.QView;
public class QMVC {

	public QMVC() {};
	public static void main(String[] args) {

		File file = new File("log.txt");
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		PrintStream ps = new PrintStream(fos);
		System.setOut(ps);
        Simulation      sim    = new Simulation();
        QView       view       = new QView(sim);
        QController controller = new QController(sim, view);

        view.setVisible(true);
    }

}