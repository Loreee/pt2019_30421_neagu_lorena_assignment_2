package Simulation;
import View.QView;
import java.util.*;
/* the simulation class. This is the main class of the application*/
public class Simulation implements Runnable {
    private int minArrivalTime;
    private int maxArrivalTime;
    private int minServiceTime;
    private int maxServiceTime;
    private int nbClients;
    private int nbQueues;
    private int timp;
    private int peakHour = 0;
    private ArrayList<Client> clients;
    private String[] clients_stack;
    private ArrayList<Queue> queues;
    private QView view;
    /*constructor
     * @param view           view ref
     * @param minArrivalTime min arrival time for clients
     * @param maxArrivalTime max arrival time for clients
     * @param minServiceTime min service time for clients
     * @param maxServiceTime max service time for clients
     * @param nbClients      number of clients in the simulation
     * @param nbQueues       number of queues in the simulation
     * @param timp           simulation time */
    public Simulation(QView view, int minArrivalTime, int maxArrivalTime, int minServiceTime, int maxServiceTime,
                      int nbClients, int nbQueues, int timp) {
        this.view = view;
        this.minArrivalTime = minArrivalTime;
        this.maxArrivalTime = maxArrivalTime;
        this.minServiceTime = minServiceTime;
        this.maxServiceTime = maxServiceTime;
        this.nbClients = nbClients;
        this.nbQueues = nbQueues;
        this.timp = timp;
        clients = new ArrayList<Client>();
        clients_stack = new String[nbClients];
        queues = new ArrayList<Queue>();
        }
    public Simulation() {}
	/** returns the emptiest queue
     * @return emptiest queue */
    public int getMinQ() {
        int min = 0;
        for (Iterator<Queue> itq = queues.iterator(); itq.hasNext(); ) {
            Queue s = itq.next();
            if (s.size() == 0)  return s.getId(); }
        for (int i = 0; i < nbQueues - 1; i++) 
            if (queues.get(i).getWaitTime() > queues.get(i + 1).getWaitTime())  min = i + 1;
        return min;
    }
    /* computes the time needed for a client until he leaves the queue
     * @param clienti     client list
     * @param curr_client the client for which the computation is done
     * @param currentTime current time
     * @return time to wait for current client*/
    public int getTimeToWait(ArrayList<Client> clienti, Client curr_client, int currentTime) {
        if (clienti.size() == 0)  return 0;
        int timeWaiting = 0;
        for (Iterator<Client> itc = clienti.iterator(); itc.hasNext(); ) {
            Client c = itc.next();
            timeWaiting += c.getServiceTime();
        }
        return timeWaiting - (currentTime - clienti.get(0).getArrivalTime());
    }
    /** randomly generates a list of clients*/
    public void generateClients() {
        int id = 0;
        while (id < nbClients) {
            int processTime = (int) ((Math.random() * (maxServiceTime - minServiceTime) + minServiceTime));
            int arrivalTime = (int) ((Math.random() * (maxArrivalTime - minArrivalTime) + minArrivalTime));
            Client cl = new Client(id++, arrivalTime, processTime);
            clients.add(cl);
        }
        Collections.sort(clients);
        System.out.println("Clients: ");
        for (Iterator<Client> itc = clients.iterator(); itc.hasNext(); ) {
            Client c = itc.next();
            System.out.println(c);
        }
        System.out.println();
    }
    /** starts all the threads associated with each queue*/
    public void startThreads() {
        int i = 0;
        while (i < nbQueues) {
            Queue newQ = new Queue(i++);
            Thread t = new Thread(newQ);
            queues.add(newQ);
            t.setDaemon(true);
            t.start();
        }
    }
    /* * displays the results into the log file*/
    public void afisare() {
        for (Iterator<Queue> itq = queues.iterator(); itq.hasNext(); ) {
            Queue s = itq.next();
            System.out.print("Queue " + s.getId() + ": ");
            for (Iterator<Client> itc = s.getClients().iterator(); itc.hasNext(); ) {
                Client c = itc.next();
                System.out.print(" " + c.getId());
            }
            System.out.println();
        }
        System.out.println();
    }
    /* checks if there are queues that are not emply
     * @return true if non-empty queue, false otherwise */
    public boolean nonEmptyQueues() {
        for (Iterator<Queue> itq = queues.iterator(); itq.hasNext(); ) {
            Queue s = itq.next();
            if (!s.isEmpty()) return true;
        }
        return false;
    }
    /* computes the average empty time of all queues and prints them
     * @return average  empty time */
    public int avgEmptyTime() {
		int average_empty_time = 0;
		for (Iterator<Queue> itq = queues.iterator(); itq.hasNext();) {
			Queue q = itq.next();
			average_empty_time += q.getEmptyTime();
			System.out.println("Queue " + q.getId() + " is empty for "+ q.getEmptyTime());
		}
		return average_empty_time / nbQueues ;
	}
    /** computes the average waiting time for all queues*/
    public void avgWaitingTime() {
        for (int i = 0; i < clients_stack.length; i++)
            System.out.println(clients_stack[i]);
    }
    /** computes the average service time for all queues */
    public void avgServiceTime() {
        for (Iterator<Queue> itc = queues.iterator(); itc.hasNext(); ) {
            Queue q = itc.next();
            System.out.println("Queue " + q.getId() + ": Average Processing Time: " + (q.getWaitTime() - 1));}
    }
    /** computes the maximum number of clients for each queue
      * @return max maxim clients */
    public int computeMaxClients() {
        int max = 0;
        for (Iterator<Queue> itq = queues.iterator(); itq.hasNext(); ) {
            Queue q = itq.next();
            max += q.size();}
        return max;
    }
    @Override
    public void run() {
        generateClients();
        startThreads();
        int i = 0, currentTime = 1, maxCl = 0, average_wait_time = 0;
        do { for (Iterator<Queue> itq = queues.iterator(); itq.hasNext();) {
				Queue q = itq.next();
				if (q.size() == 0)
					q.setEmptyTime();}
			for (Iterator<Client> itc = clients.iterator(); itc.hasNext();) {
				Client cl = itc.next();
				if (currentTime != cl.getArrivalTime()) break;
				itc.remove();
				int chosen;
				if (queues.size() == 0) chosen = 0; else chosen = getMinQ();
				cl.setFinishTime(currentTime + getTimeToWait(queues.get(chosen).getClients(), cl, currentTime)+ cl.getServiceTime());
				cl.setWaitingTime(getTimeToWait(queues.get(chosen).getClients(), cl, currentTime));
				clients_stack[i++] = ("client " + cl.getId() + " waits for " + cl.getWaitingTime() + " and finishes at  "+ cl.getFinishTime());
				average_wait_time += cl.getWaitingTime();
				queues.get(chosen).addClient(cl); // place client in the smallest queue
				int maxNbCQ = computeMaxClients();
				if (maxCl < maxNbCQ) { maxCl = maxNbCQ; peakHour = currentTime; }
			}
			try {Thread.sleep(10);} catch (InterruptedException e1) {e1.printStackTrace();}
			view.setQueues(queues);
			System.out.println("Time : " + currentTime++);
			afisare();
			try {Thread.sleep(1000);} catch (InterruptedException e) {System.out.println(e.getMessage());}
		} while (currentTime < timp && !clients.isEmpty());
		while (nonEmptyQueues()) {
			for (Iterator<Queue> itq = queues.iterator(); itq.hasNext();) {
				Queue q = itq.next();
				if (q.size() == 0)
					q.setEmptyTime();
			}
			System.out.println("Time : " + currentTime++);
			afisare();
			view.setQueues(queues);
			try {Thread.sleep(1000);} catch (InterruptedException e) {System.out.println(e.getMessage());}
		}
		System.out.println("Time : " + currentTime);
		afisare();
		view.setQueues(queues);
		System.out.println("Average Empty Time : " + avgEmptyTime());
		System.out.println();
		avgWaitingTime();
		System.out.println("Average Waiting Time : " + average_wait_time / i);
		System.out.println();
		avgServiceTime();
		System.out.println("Peak Hour :" + peakHour);
    }
}