package Exceptions;

/**
 * exception thrown if input parsing fails
 */
public class WrongValuesException extends Exception {

    public WrongValuesException(String msg) {
        super(msg);
    }

}

