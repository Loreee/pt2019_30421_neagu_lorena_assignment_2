package View;

import Simulation.Queue;
import Simulation.Simulation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * GUI View
 */
public class QView extends JFrame {

    protected Simulation simulation;
    private JButton start = new JButton("Start Simulation");
    private JButton enter = new JButton("Enter Data");

    private JLabel text_min_arrival = new JLabel("Minimum arrival time");
    public JTextField min_arrival = new JTextField(3);
    private JLabel text_max_arrival = new JLabel("Maximum arrival time");
    public JTextField max_arrival = new JTextField(3);

    private JLabel text_min_processing = new JLabel("Minimum processing time");
    public JTextField min_processing = new JTextField(3);
    private JLabel text_max_processing = new JLabel("Maximum processing time");
    public JTextField max_processing = new JTextField(3);

    private JLabel text_nr_clients = new JLabel("Number of clients");
    public JTextField nr_clients = new JTextField(3);
    private JLabel text_nr_servers = new JLabel("Number of queues");
    public JTextField nr_servers = new JTextField(3);

    private JLabel[] qs = new JLabel[7];

    private JLabel timp = new JLabel("Time of Simulation ");

    public JTextField[] qs_text = new JTextField[7];
    public JTextField incrementTime = new JTextField();

    public static int numberservers;
    public static int numberClients;

    /**
     * constructor
     *
     * @param sim simulation ref
     */
    public QView(Simulation sim) {
        this.simulation = sim;
        this.setTitle("Queue Simulation");
        this.getContentPane().setBackground(Color.yellow);
        JPanel values = new JPanel(new GridLayout(4, 4, 40, 5));
        values.add(text_min_arrival);
        values.add(min_arrival);
        values.add(text_max_arrival);
        values.add(max_arrival);
        values.add(text_min_processing);
        values.add(min_processing);
        values.add(text_max_processing);
        values.add(max_processing);
        values.add(text_nr_clients);
        values.add(nr_clients);
        values.add(text_nr_servers);
        values.add(nr_servers);
        values.add(timp);
        values.add(incrementTime);
        values.add(enter);
        values.add(start);

        JPanel queues = new JPanel(new GridLayout(7, 2, 20, 10));
        for (int i = 1; i <= 7; i++) {
            qs[i - 1] = new JLabel("Queue " + (i));
            qs[i - 1].setBackground(Color.GRAY);
            queues.add(qs[i - 1]);
            qs_text[i - 1] = new JTextField();
            queues.add(qs_text[i - 1]);
        }
        queues.setBorder(BorderFactory.createEmptyBorder(20, 100, 30, 100));
        queues.setPreferredSize(new Dimension(1000, 400));
        JPanel content = new JPanel();
        content.setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));
        content.add(values);
        JPanel emptycontent = new JPanel();
        emptycontent.setPreferredSize(new Dimension(1, 10));
        content.add(emptycontent);
        content.add(queues);

        content.setBorder(BorderFactory.createEmptyBorder(20, 30, 20, 30));
        content.setPreferredSize(new Dimension(500, 700));
        this.setPreferredSize(new Dimension(800, 700));
        this.setContentPane(content);
        this.pack();

    }

    /**
     * adds listener for enter button
     *
     * @param ent implementation of action listener
     */
    public void addEnterListener(ActionListener ent) {
        enter.addActionListener(ent);
    }

    /**
     * adds listener for start button
     *
     * @param st implementation of action listener
     */
    public void addStartListener(ActionListener st) {
        start.addActionListener(st);
    }

    /**
     * initializez all queues of the gui
     *
     * @param nb_servers number of servers
     * @return list of empty textfields
     */
    public JTextField[] initializeQueues(int nb_servers) {
        for (int i = 0; i < nb_servers; i++) {
            qs_text[i].setText("");
            qs[i].setBackground(Color.GREEN);
        }
        return qs_text;
    }

    /**
     * updated the status of the queues
     *
     * @param cozi list of all queues
     * @return list of textfields representing the clients in each queue
     */
    public JTextField[] setQueues(ArrayList<Queue> cozi) {

        int i = 0;
        for (Iterator<Queue> itq = cozi.iterator(); itq.hasNext(); ) {
            Queue q = itq.next();
            qs_text[i++].setText(q.toString());
        }

        return qs_text;
    }

    /**
     * displays an error
     *
     * @param errMessage error message
     */
    public void showError(String errMessage) {
        JOptionPane.showMessageDialog(this, errMessage);
    }
}
